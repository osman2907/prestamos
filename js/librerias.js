consultarPHP=function(url,datos,tipo,async){
    var retorno;
    var objeto=$.ajax({
        async: async,
        type: "POST",
        url: url,
        dataType: tipo,
        data:datos,
        success: function(respuesta){
            retorno=respuesta;
        },
        error: function (xhr, ajaxOptions, thrownError){
	        retorno="error";
      	},
    });
    return retorno;
};

$(document).on('keyup','.solo-numero',function (){
    this.value = (this.value + '').replace(/[^0-9]/g,'');
});


$(document).on('keyup','.minuscula',function (){
    this.value = this.value.toLowerCase();
});

$(document).on('keyup','.mayuscula',function (){
    this.value = this.value.toUpperCase();
});
