<?php
class Prestamos extends CActiveRecord{
	public $identificacion;
	public $nombreCompleto;
	public $cuotas_canceladas;
	public $cuotas_faltantes;
	public $monto_cancelado;
	public $monto_faltante;

	
	public function tableName(){
		return 'prestamos';
	}

	
	public function rules(){
		return array(
			array('id_solicitante, monto, cantidad_cuotas, fecha_registro, usuario_registro, id_estatus', 'required'),
			array('id_solicitante, cantidad_cuotas, usuario_registro, usuario_autorizacion, id_estatus, usuario_entrega', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			array('fecha_autorizacion, fecha_tentativa_entrega, fecha_entrega,fecha_rechazo, usuario_rechazo', 'safe'),

			array('id_prestamo, id_solicitante, monto, cantidad_cuotas, fecha_registro, fecha_autorizacion, usuario_registro, usuario_autorizacion, id_estatus, fecha_tentativa_entrega, fecha_entrega, usuario_entrega, fecha_rechazo, usuario_rechazo', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idEstatus' => array(self::BELONGS_TO, 'PrestamosEstatus', 'id_estatus'),
			'idSolicitante' => array(self::BELONGS_TO, 'Solicitantes', 'id_solicitante'),
			'idUsuarioRegistro' => array(self::BELONGS_TO, 'CrugeUserModel', 'usuario_registro'),
			'idUsuarioAutorizacion' => array(self::BELONGS_TO, 'CrugeUserModel', 'usuario_autorizacion'),
			'idUsuarioEntrega' => array(self::BELONGS_TO, 'CrugeUserModel', 'usuario_entrega'),
			'prestamosCuotas'=>array(self::HAS_MANY, 'PrestamosCuotas', 'id_prestamo'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_prestamo' => 'Nro.',
			'id_solicitante' => 'Id Solicitante',
			'monto' => 'Monto Préstamo',
			'cantidad_cuotas' => 'Cantidad Cuotas',
			'fecha_registro' => 'Fecha Registro',
			'fecha_autorizacion' => 'Fecha Autorizacion',
			'usuario_registro' => 'Usuario Registro',
			'usuario_autorizacion' => 'Usuario Autorizacion',
			'id_estatus' => 'Estatus',
			'fecha_tentativa_entrega' => 'Fecha Tentativa Entrega',
			'fecha_entrega' => 'Fecha Entrega',
			'usuario_entrega' => 'Usuario Entrega',
			'identificacion' => 'Identificación'
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_prestamo',$this->id_prestamo);
		$criteria->compare('id_solicitante',$this->id_solicitante);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('cantidad_cuotas',$this->cantidad_cuotas);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('fecha_autorizacion',$this->fecha_autorizacion,true);
		$criteria->compare('usuario_registro',$this->usuario_registro);
		$criteria->compare('usuario_autorizacion',$this->usuario_autorizacion);
		$criteria->compare('id_estatus',$this->id_estatus);
		$criteria->compare('fecha_tentativa_entrega',$this->fecha_tentativa_entrega,true);
		$criteria->compare('fecha_entrega',$this->fecha_entrega,true);
		$criteria->compare('usuario_entrega',$this->usuario_entrega);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function ordenarRegistro($post){
		$post['Prestamos']['fecha_registro']=date("Y-m-d H:i:s");
		$post['Prestamos']['usuario_registro']=Yii::app()->user->id;
		$post['Prestamos']['id_estatus']=1;
		return $post;
	}


	public function ordenarAutorizacion($post){
		$post['Prestamos']['fecha_autorizacion']=date("Y-m-d H:i:s");
		$post['Prestamos']['usuario_autorizacion']=Yii::app()->user->id;
		$post['Prestamos']['id_estatus']=2;
		$post['Prestamos']['fecha_tentativa_entrega']=date("Y-m-d",strtotime('+7 day', strtotime(date("Y-m-d"))));
		return $post;
	}

	public function ordenarRechazo($post){
		$post['Prestamos']['fecha_rechazo']=date("Y-m-d H:i:s");
		$post['Prestamos']['usuario_rechazo']=Yii::app()->user->id;
		$post['Prestamos']['id_estatus']=5;
		return $post;
	}


	public function ordenarEntrega($post){
		$post['Prestamos']['fecha_entrega']=date("Y-m-d H:i:s");
		$post['Prestamos']['usuario_entrega']=Yii::app()->user->id;
		$post['Prestamos']['id_estatus']=3;
		return $post;
	}


	public function montoCuotas(){
		return $this->monto/$this->cantidad_cuotas;	
	}

	public function getCuotasFaltantes(){
		$coutasCanceladas=count($this->prestamosCuotas);
		return $this->cantidad_cuotas-$coutasCanceladas;
	}


	public function validarPagoTotal(){
		$cantidadCuotas=$this->cantidad_cuotas;
		$cuotasPagas=count($this->prestamosCuotas)+1;

		if($cuotasPagas >= $cantidadCuotas){
			$this->fecha_pagado=date("Y-m-d H:i:s");
			$this->id_estatus=4;
			$this->save();
			return true;
		}
		return false;
	}


	public function getMontoCancelado(){
		$cuotas=$this->prestamosCuotas;
		$pagado=0;
		foreach ($cuotas as $cuota){
			$monto=$cuota->monto;
			$pagado+=$monto;
		}
		return $pagado;
	}


	public function getMontoFaltante(){
		$pagado=$this->getMontoCancelado();
		return $this->monto-$pagado;
	}


	public function getDetallesCuotas(){
		$cantidadCuotas=$this->cantidad_cuotas;
		$fechaEntrega=$this->fecha_entrega;
		$pagos=$this->prestamosCuotas;
		$cuotas=array();
		for($i=1; $i<=$cantidadCuotas; $i++){
			$fechaEntrega=date("Y-m-d",strtotime('+30 day', strtotime($fechaEntrega)));
			$cuotas[$i]=array(
				'monto'=>number_format($this->montoCuotas(), 2, ',', '.'),
				'fechaMaxima'=>date("d/m/Y",strtotime($fechaEntrega))
			);
			if(isset($pagos[$i-1])){
				$cuotas[$i]['fechaPago']=date("d/m/Y h:i a",strtotime($pagos[$i-1]->fecha_pago));
				$cuotas[$i]['usuario']=$pagos[$i-1]->idUsuarioRegistro->username;
				$cuotas[$i]['diasRetraso']=$this->getDiasRetraso($fechaEntrega,$pagos[$i-1]->fecha_pago);
			}
		}
		return $cuotas;
	}


	public function getDiasRetraso($fechaMaxima,$fechaPago){
		if($fechaMaxima >= $fechaPago){
			return 0;
		}else{
			$inicio = strtotime($fechaMaxima);
		    $fin = strtotime($fechaPago);
		    $dif = $fin - $inicio;
		    $dias = (( ( $dif / 60 ) / 60 ) / 24);
		    return ceil($dias)-1;
		}
	}


	public function getPrestadoActual(){
		$sql = "SELECT SUM(monto) as prestado_actual FROM prestamos WHERE id_estatus IN(1,2,3)";
		$results = Yii::app()->db->createCommand($sql)->queryAll();
		return (int)$results[0]["prestado_actual"];
	}

	public function getMontoDisponible(){
		$prestadoActual=$this->getPrestadoActual();
		$montoMaximo=Configuraciones::model()->findByPk(1)->maximo_prestamos;
		return $montoMaximo-$prestadoActual;
	}


	public function validarDisponible(){
		$montoPrestamo=$this->monto;
		$montoDisponible=$this->getMontoDisponible();
		return $montoPrestamo > $montoDisponible?false:true;
	}

}
