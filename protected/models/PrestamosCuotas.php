<?php
class PrestamosCuotas extends CActiveRecord{
	
	public function tableName(){
		return 'prestamos_cuotas';
	}

	
	public function rules(){
		return array(
			array('id_prestamo, monto, fecha_pago, usuario_registro', 'required'),
			array('id_prestamo, usuario_registro', 'numerical', 'integerOnly'=>true),
			array('monto', 'numerical'),
			array('id_prestamo_cuota, id_prestamo, monto, fecha_pago, usuario_registro', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'idPrestamo' => array(self::BELONGS_TO, 'Prestamos', 'id_prestamo'),
			'idUsuarioRegistro' => array(self::BELONGS_TO, 'CrugeUserModel', 'usuario_registro')
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_prestamo_cuota' => 'Id Prestamo Cuota',
			'id_prestamo' => 'Id Prestamo',
			'monto' => 'Monto Cuota',
			'fecha_pago' => 'Fecha Pago',
			'usuario_registro' => 'Usuario Registro',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_prestamo_cuota',$this->id_prestamo_cuota);
		$criteria->compare('id_prestamo',$this->id_prestamo);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('fecha_pago',$this->fecha_pago,true);
		$criteria->compare('usuario_registro',$this->usuario_registro);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function ordenarPago($post){
		$post['PrestamosCuotas']['fecha_pago']=date("Y-m-d H:i:s");
		$post['PrestamosCuotas']['usuario_registro']=Yii::app()->user->id;
		return $post;
	}

}
