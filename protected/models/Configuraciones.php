<?php
class Configuraciones extends CActiveRecord{
	
	public function tableName(){
		return 'configuraciones';
	}

	
	public function rules(){
		return array(
			array('id_configuracion, maximo_cuotas, maximo_prestamos', 'required'),
			array('id_configuracion, maximo_cuotas', 'numerical', 'integerOnly'=>true),
			array('maximo_prestamos', 'numerical'),
			array('id_configuracion, maximo_cuotas, maximo_prestamos', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_configuracion' => 'Id Configuracion',
			'maximo_cuotas' => 'Máximo Cuotas',
			'maximo_prestamos' => 'Máximo Prestamos',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_configuracion',$this->id_configuracion);
		$criteria->compare('maximo_cuotas',$this->maximo_cuotas);
		$criteria->compare('maximo_prestamos',$this->maximo_prestamos);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function listaCuotas(){
		$modelConfiguraciones=$this->findByPk(1);
		$cantidad=$modelConfiguraciones->maximo_cuotas;
		$lista=array();
		for($i=1; $i<=$cantidad; $i++){
			$lista[$i]=$i;
		}
		return $lista;
	}

}
