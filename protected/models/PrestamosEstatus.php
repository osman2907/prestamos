<?php
class PrestamosEstatus extends CActiveRecord{
	
	public function tableName(){
		return 'prestamos_estatus';
	}

	
	public function rules(){
		return array(
			array('id_estatus, estatus', 'required'),
			array('id_estatus', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>100),
			array('id_estatus, estatus', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'prestamoses' => array(self::HAS_MANY, 'Prestamos', 'id_estatus'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_estatus' => 'Id Estatus',
			'estatus' => 'Estatus',
		);
	}

	
	public function search(){

		$criteria=new CDbCriteria;

		$criteria->compare('id_estatus',$this->id_estatus);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getListaEstatus(){
   		$listaEstatus=$this->findAll(array('order'=>'id_estatus ASC'));
		return CHtml::listData($listaEstatus,'id_estatus','estatus');
   	
	}

}
