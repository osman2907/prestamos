<?php
class Reportes{

	public function getListaAnios(){
		$inicio=2010;
		$lista=array();
		for($i=date("Y"); $i>=$inicio; $i--){
			$lista[]=$i;
		}
		return $lista;
	}

	public function getListaMeses(){
		return array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	}

	public function getGraficoCantidades($anio){
		$data[]=$this->getValoresGrafico($anio,"Pendiente","fecha_registro","COUNT","id_prestamo");
		$data[]=$this->getValoresGrafico($anio,"Autorizados","fecha_autorizacion","COUNT","id_prestamo");
		$data[]=$this->getValoresGrafico($anio,"Entregados","fecha_entrega","COUNT","id_prestamo");
		$data[]=$this->getValoresGrafico($anio,"Pagados","fecha_pagado","COUNT","id_prestamo");
		$data[]=$this->getValoresGrafico($anio,"Rechazados","fecha_rechazo","COUNT","id_prestamo");
		return $data;
	}

	public function getGraficoMontos($anio){
		$data[]=$this->getValoresGrafico($anio,"Solicitados","fecha_registro","SUM","monto");
		return $data;
	}

	public function getValoresGrafico($anio,$titulo,$campo,$funcion,$campoCuenta){
		$sql = "
			SELECT $funcion($campoCuenta) AS 'cantidad',MONTH($campo) AS 'mes'
			FROM prestamos
			WHERE YEAR($campo) = $anio
			GROUP BY MONTH($campo)
			ORDER BY MONTH($campo) ASC
		";
		$respuesta = Yii::app()->db->createCommand($sql)->queryAll();
		$i=0;
		$retorno['data']=array();
		$retorno['name']=$titulo;
		foreach ($respuesta as $registro){
			$retorno['data'][$registro['mes']-1]=(int)$registro['cantidad'];
		}

		$retorno['data']=$this->completarMeses($retorno['data']);
		return $retorno;
	}


	public function completarMeses($data){
		for($i=0; $i<12; $i++){
			if(!isset($data[$i])){
				$data[$i]=0;
			}
		}
		ksort($data);
		return $data;
	}
}