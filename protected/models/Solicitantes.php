<?php
class Solicitantes extends CActiveRecord{
	
	public function tableName(){
		return 'solicitantes';
	}

	
	public function rules(){
		return array(
			array('identificacion, primer_nombre, primer_apellido, telefono_local, telefono_movil, email', 'required'),
			array('email','email'),
			array('identificacion', 'numerical', 'integerOnly'=>true),
			array('primer_nombre, primer_apellido', 'length', 'max'=>100),
			array('telefono_local, telefono_movil', 'length', 'max'=>20),
			array('email', 'length', 'max'=>300),
			array('id_solicitante, identificacion, primer_nombre, primer_apellido, telefono_local, telefono_movil, email', 'safe', 'on'=>'search'),
		);
	}

	
	public function relations(){
		return array(
			'prestamoses' => array(self::HAS_MANY, 'Prestamos', 'id_solicitante'),
		);
	}

	
	public function attributeLabels(){
		return array(
			'id_solicitante' => 'Id',
			'identificacion' => 'Identificación',
			'primer_nombre' => 'Primer Nombre',
			'primer_apellido' => 'Primer Apellido',
			'telefono_local' => 'Teléfono Local',
			'telefono_movil' => 'Teléfono Móvil',
			'email' => 'Correo Electrónico',
		);
	}

	
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id_solicitante',$this->id_solicitante);
		$criteria->compare('identificacion',$this->identificacion);
		$criteria->compare('primer_nombre',$this->primer_nombre,true);
		$criteria->compare('primer_apellido',$this->primer_apellido,true);
		$criteria->compare('telefono_local',$this->telefono_local,true);
		$criteria->compare('telefono_movil',$this->telefono_movil,true);
		$criteria->compare('email',$this->email,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public static function model($className=__CLASS__){
		return parent::model($className);
	}


	public function getNombreCompleto(){
		return $this->primer_nombre." ".$this->primer_apellido;
	}

}
