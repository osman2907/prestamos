<style type="text/css">
	div.form .form-group {
		border: 0px !important;
		box-shadow: none !important;
		padding: 0px !important;
	}
</style>

<?php
/*$this->widget(
    'booster.widgets.TbBreadcrumbs',
    array('links' => array(
    	'Inicio de Sesión')
    )
);*/
?>

<h1><?php //echo CrugeTranslator::t('logon',"Login"); ?></h1>
<?php if(Yii::app()->user->hasFlash('loginflash')): ?>
<div class="flash-error">
	<?php echo Yii::app()->user->getFlash('loginflash'); ?>
</div>
<?php else: ?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'logon-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>
	<div>
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					<img class='imagen-login' src="<?php echo Yii::app()->request->baseUrl;?>/images/logo.png">
				</div>
			</div>


			<div class="row">
				<div class="col-md-4 col-md-offset-4">
			        <div class="formulario-login">

						<div class="form-group has-feedback has-feedback-left">
							<?php echo $form->labelEx($model,'username'); ?>
							<?php echo $form->textField($model,'username',array('class'=>'form-control')); ?>
					        <i class="form-control-feedback glyphicon glyphicon-user"></i>
					        <?php echo $form->error($model,'username'); ?>
					    </div>

					    <div class="form-group has-feedback has-feedback-left">
							<?php echo $form->labelEx($model,'password'); ?>
							<?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
					        <i class="form-control-feedback glyphicon glyphicon-lock"></i>
					        <?php echo $form->error($model,'password'); ?>
					    </div>

						<!--<div class="row rememberMe">
							<?php echo $form->checkBox($model,'rememberMe'); ?>
							<?php echo $form->label($model,'rememberMe'); ?>
							<?php echo $form->error($model,'rememberMe'); ?>
						</div>-->

						<div class="row buttons" style="padding-top:20px;">
							<?php echo CHtml::submitButton('Iniciar Sesión',array('class'=>'btn btn-primary')); ?>
							<?php //Yii::app()->user->ui->tbutton(CrugeTranslator::t('logon', "Login")); ?>
							<?php //echo Yii::app()->user->ui->passwordRecoveryLink; ?>
							<?php
								//if(Yii::app()->user->um->getDefaultSystem()->getn('registrationonlogin')===1)
									//echo Yii::app()->user->ui->registrationLink;
							?>
						</div>

						<?php
							//	si el componente CrugeConnector existe lo usa:
							//
							if(Yii::app()->getComponent('crugeconnector') != null){
							if(Yii::app()->crugeconnector->hasEnabledClients){ 
						?>
						<div class='crugeconnector'>
							<span><?php echo CrugeTranslator::t('logon', 'You also can login with');?>:</span>
							<ul>
							<?php 
								$cc = Yii::app()->crugeconnector;
								foreach($cc->enabledClients as $key=>$config){
									$image = CHtml::image($cc->getClientDefaultImage($key));
									echo "<li>".CHtml::link($image,
										$cc->getClientLoginUrl($key))."</li>";
								}
							?>
							</ul>
						</div>
						<?php }} ?>
					</div>
				</div>
			</div>
	</div>

<?php $this->endWidget(); ?>
</div>
<?php endif; ?>
