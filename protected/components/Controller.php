<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public function getExtension($archivo){
        $partes=explode(".",$archivo);
        $extension=$partes[count($partes)-1];
        return $extension;
    }


	public function cambiarFormatoFecha($fecha){
		if(empty($fecha)){
			return $fecha;
		}
        if(strpos($fecha,"/")){ //Si está en formato dd/mm/yyyy lo convierte a yyyy-mm-dd
            $fecha=explode("/",$fecha);
            $fecha=$fecha[2]."-".$fecha[1]."-".$fecha[0];
        }else{ //Si está en formato yyyy-mm-dd lo convierte a dd/mm/yyyy
            $fecha=explode("-",$fecha);
            $fecha=$fecha[2]."/".$fecha[1]."/".$fecha[0];
        }
        return $fecha;
    }


    public function obtenerEventos(){
        $listado=Eventos::model()->findAll(array('condition'=>"id_estatus=1"));

        foreach ($listado as $list){
            $eventos[]=array(
                'id'=>$list->id_evento,
                'title'=>$list->titulo,
                'start'=>$list->fecha_inicio,
                'end'=>date("Y-m-d",strtotime('+1 day',strtotime($list->fecha_fin)))
            );
        }

        return json_encode($eventos);
    }


    public function beforeAction($action){
        //Yii::log(__METHOD__ . ' isGuest: ' . (Yii::app()->getUser()->isGuest ? 'Si' : 'No'));

        // Cada vez que se ejecuta una acción se actualiza el tiempo de expiración
        // TODO: integrar con cruge o mejorar
        $sys = Yii::app()->user->um->getDefaultSystem();
        $duration = $sys->getn('sessionmaxdurationmins');
        // Encuentra la última sesión y actualiza la fecha de expiración
        $model = CrugeSession::model()->findLast(Yii::app()->user->id);
        if ($model != null) {
            $model->expire = CrugeUtil::makeExpirationDateTime($duration);
            $model->save();
        }

        return true;
    }

    public function obtenerNoticias(){
        $opciones['condition']="id_estatus=1";
        $noticias=Noticias::model()->findAll($opciones);
        return $noticias;
    }

    public function obtenerCarrusel(){
        $opciones['order']="id_carrusel";
        $carrusel=Carrusel::model()->findAll($opciones);
        return $carrusel;
    }

    public function obtenerRedesSociales(){
        $redesSociales=RedesSociales::model()->findAll();
        return $redesSociales;
    }

    public function enviarCorreo($para,$asunto,$mensaje){
        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $cabeceras .= 'From: Sistema de Gestión de Préstamos <admin@ilernus.com>' . "\r\n";
        mail($para, $asunto, $mensaje, $cabeceras);
    }
}




