<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print">
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css">

	<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery/jquery-3.1.0.min.js"></script>

	

	<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/librerias.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<script type="text/javascript">
		$(document).ready(function(){
			setInterval(function(){ $(".flash-mensaje").remove(); }, 6000);
		});
	</script>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo">
			<img class="logo-cabecera" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.png"/>
			<?php echo CHtml::encode(Yii::app()->name); ?>
		</div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Inicio', 'url'=>array('/site/index')),
				array('label'=>'Iniciar Sesión', 'url'=>array('/cruge/ui/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Préstamos', 'url'=>array('/prestamos/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Solicitantes', 'url'=>array('/solicitantes/admin'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Reportes', 'url'=>array('/reportes/index'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Configuraciones', 'url'=>array('/configuraciones/index'), 'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Gestión de Usuarios', 'url'=>array('/cruge/ui/usermanagementadmin'), 'visible'=>!Yii::app()->user->isGuest),

				//array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('/site/contact')),
				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Cerrar Sesión ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php
    foreach(Yii::app()->user->getFlashes() as $key => $message){
        echo '<div class="flash-mensaje flash-' . $key . '">' . $message . "</div>\n";
    }
	?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<strong>EVALUCIÓN ILERNUS <?php echo date("Y") ?></strong>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
