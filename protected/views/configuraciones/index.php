<?php
$this->breadcrumbs=array(
	'Configuraciones',
);
?>

<h1>Configuraciones</h1>

<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'prestamos-form',
		'enableAjaxValidation'=>false,
	)); ?>

		<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

		<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'maximo_cuotas'); ?>
				<?php echo $form->textField($model,'maximo_cuotas',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				<?php echo $form->labelEx($model,'maximo_prestamos'); ?>
				<?php echo $form->textField($model,'maximo_prestamos',array('class'=>'form-control')); ?>
			</div>
		</div>

		<div class="row">
		<div class="col-md-12">
			<?php echo CHtml::submitButton('Guardar',array('class'=>'btn btn-success')); ?>
		</div>
	</div>

	<?php $this->endWidget(); ?>

</div>