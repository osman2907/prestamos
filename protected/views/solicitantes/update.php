<?php
/* @var $this SolicitantesController */
/* @var $model Solicitantes */

$this->breadcrumbs=array(
	'Listado de Solicitantes'=>array('admin'),
	'Modificar Solicitantes',
);

$this->menu=array(
	array('label'=>'Listado de Solicitantes', 'url'=>array('admin')),
	array('label'=>'Registrar Solicitantes', 'url'=>array('create')),
	array('label'=>'Consultar Solicitantes', 'url'=>array('view', 'id'=>$model->id_solicitante)),
);
?>

<h1>Modificar Solicitantes</h1>

<?php $this->renderPartial('_form', compact('model')); ?>