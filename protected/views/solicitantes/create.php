<?php
$this->breadcrumbs=array(
	'Solicitantes'=>array('admin'),
	'Registrar Solicitantes',
);

$this->menu=array(
	array('label'=>'Listado de Solicitantes', 'url'=>array('admin')),
);
?>

<h1>Registrar Solicitantes</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>