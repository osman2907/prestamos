<?php
/* @var $this SolicitantesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Solicitantes',
);

$this->menu=array(
	array('label'=>'Create Solicitantes', 'url'=>array('create')),
	array('label'=>'Manage Solicitantes', 'url'=>array('admin')),
);
?>

<h1>Solicitantes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
