<?php
$this->breadcrumbs=array(
	'Listado de Solicitantes',
);


$this->menu=array(
	array('label'=>'Registrar Solicitantes', 'url'=>array('create')),
);
?>


<h1>Listado de Solicitantes</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'solicitantes-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_solicitante',
		'identificacion',
		'primer_nombre',
		'primer_apellido',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
