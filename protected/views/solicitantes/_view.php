<?php
/* @var $this SolicitantesController */
/* @var $data Solicitantes */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_solicitante')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_solicitante), array('view', 'id'=>$data->id_solicitante)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('identificacion')); ?>:</b>
	<?php echo CHtml::encode($data->identificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primer_nombre')); ?>:</b>
	<?php echo CHtml::encode($data->primer_nombre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primer_apellido')); ?>:</b>
	<?php echo CHtml::encode($data->primer_apellido); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_local')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_local); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_movil')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_movil); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />


</div>