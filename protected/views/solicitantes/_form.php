<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'solicitantes-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<div class="row">
		<div class="col-md-12">
			<?php echo $form->errorSummary($model); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<?php echo $form->labelEx($model,'identificacion'); ?>
			<?php echo $form->textField($model,'identificacion',array('maxlength'=>9,'class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'identificacion'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'primer_nombre'); ?>
			<?php echo $form->textField($model,'primer_nombre',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'primer_nombre'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'primer_apellido'); ?>
			<?php echo $form->textField($model,'primer_apellido',array('size'=>60,'maxlength'=>100,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'primer_apellido'); ?>
		</div>
	</div>

	

	<div class="row">
		<div class="col-md-3">
			<?php echo $form->labelEx($model,'telefono_local'); ?>
			<?php echo $form->textField($model,'telefono_local',array('size'=>20,'maxlength'=>20,'class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'telefono_local'); ?>
		</div>

		<div class="col-md-3">
			<?php echo $form->labelEx($model,'telefono_movil'); ?>
			<?php echo $form->textField($model,'telefono_movil',array('size'=>20,'maxlength'=>20,'class'=>'form-control solo-numero')); ?>
			<?php echo $form->error($model,'telefono_movil'); ?>
		</div>

		<div class="col-md-6">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>300,'class'=>'form-control')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
	</div>

	

	

	<div class="row">
		<div class="col-md-4">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->