<?php
$this->breadcrumbs=array(
	'Solicitantes'=>array('admin'),
	'Consultar Solicitantes'
);

$this->menu=array(
	array('label'=>'Listado de Solicitantes', 'url'=>array('admin')),
	array('label'=>'Registrar Solicitantes', 'url'=>array('create')),
	array('label'=>'Modificar Solicitantes', 'url'=>array('update', 'id'=>$model->id_solicitante)),
	array('label'=>'Eliminar Solicitantes', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_solicitante),'confirm'=>'¿Seguro que desea eliminar este solicitante?')),
);
?>

<h1>Consultar Solicitantes</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_solicitante',
		'identificacion',
		'primer_nombre',
		'primer_apellido',
		'telefono_local',
		'telefono_movil',
		'email',
	),
)); ?>
