<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click","#btn-consultar",function(){
			anio=$("#anio").val();
			$("#contenido-grafica").html('');
			if(anio == ''){
				bootbox.alert("Por favor seleccione el año a consultar");
				return false;
			}
		})
	});
</script>

<?php
$this->breadcrumbs=array(
	'Reportes Gráficos',
);
?>
<h1>Reportes Gráficos</h1>

<div class="alert alert-info">Por favor seleccione el año a consultar para ver el detalle de los préstamos desglosado mensualmente.</div>

<form method="post">
	<div class="row">
		<div class="col-md-3">
			<label>Año</label>
			<select id="anio" name="anio" class="form-control">
				<option value="">Seleccione</option>
				<?php
				foreach ($listaAnios as $anio){
					echo "<option value='$anio'>$anio</option>";
				}
				?>
			</select>
		</div>

		<div class="col-md-3">
			<input id="btn-consultar" type="submit" name="consultar" value="Consultar" class="btn btn-success sin-etiqueta">
		</div>
	</div>
</form>

<div class="row">
	<div id="contenido-grafica" class="col-md-12">
		<?php
		if(isset($data)){
			$this->Widget('ext.highcharts.highcharts.HighchartsWidget', array(
			   	'options' => array(
			   		'chart'=>array('type'=>'column'),
			    	'title' => array('text' => 'Estadística de cantidad de préstamos desglosados mensualmente por estatus'),
			    	'subtitle' => array('text' => "Año: $anioReporte"),
			      	'xAxis' => array(
			      		'title' => array('text' => 'Meses'),
			        	'categories' => $listaMeses
			        ),
			      	'yAxis' => array(
			        	'title' => array('text' => 'Cantidades')
			      	),
			      	'series' => $data
			   	)
			));
		}


		if(isset($dataMontos)){
			echo "<br><br>";
			$this->Widget('ext.highcharts.highcharts.HighchartsWidget', array(
			   	'options' => array(
			   		'chart'=>array('type'=>'column'),
			    	'title' => array('text' => 'Estadística de montos solicitados mensualmente'),
			    	'subtitle' => array('text' => "Año: $anioReporte"),
			      	'xAxis' => array(
			      		'title' => array('text' => 'Meses'),
			        	'categories' => $listaMeses
			        ),
			      	'yAxis' => array(
			        	'title' => array('text' => 'Cantidades')
			      	),
			      	'series' => $dataMontos
			   	)
			));
		}
		?>
	</div>
</div>

	