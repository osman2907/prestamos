<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on("click","#buscar",function(){
			identificacion=$("#identificacion").val();
			if(identificacion == ''){
				bootbox.alert("Por favor ingresar el número de identificación");
				return false;
			}

			if(identificacion.length < 6 ){
				bootbox.alert("El número de identificación debe contener mínimo 6 dígitos");
				return false;
			}

			$.ajax({
				type:'post',
				async: true,
				url: "<?php echo $this->createUrl("/prestamos/buscarSolicitante") ?>",
				dataType:"html",
				data:$("form").serialize(),
				success:function(idSolicitante){
					if(!idSolicitante){
						bootbox.confirm("¿El solicitante no existe, desea registrarlo?",function(respuesta){
							if(respuesta){
								$("form").attr("action","../solicitantes/create?identificacion="+identificacion);
								$("form").submit();
							}
						});
					}else{
						$("form").attr("action","create?idSolicitante="+idSolicitante);
						$("form").submit();
					}
				},
				error:function(){
					bootbox.alert("Ha ocurrido un error");
				}
			});

			return false;
		});

	});
</script>

<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'=>array('admin'),
	'Registrar Préstamos',
);

$this->menu=array(
	array('label'=>'Estatus General', 'url'=>array('general')),
	array('label'=>'Listado de Préstamos', 'url'=>array('admin'))
);
?>

<h1>Registrar Préstamos</h1>

<form method="post">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info">Por favor ingrese el número de identificación del solicitante.</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<label>Identificación:</label>
			<input id="identificacion" type="text" name="identificacion" class="form-control solo-numero">
		</div>

		<div class="col-md-4">
			<input id="buscar" type="submit" name="buscar" value="Buscar" class="btn btn-success sin-etiqueta">
		</div>
	</div>
</form>