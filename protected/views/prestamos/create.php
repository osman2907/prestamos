<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'=>array('admin'),
	'Registrar Préstamos',
);

$this->menu=array(
	array('label'=>'Estatus General', 'url'=>array('general')),
	array('label'=>'Listado de Préstamos', 'url'=>array('admin')),
);
?>

<h1>Registrar Préstamos</h1>

<?php
echo CHtml::link(
	'Regresar',
	array('create'),
	array('class'=>'btn btn-danger'));
?>

<br><br>
<?php $this->renderPartial('_form', compact('model','modelSolicitante','listaCuotas')); ?>