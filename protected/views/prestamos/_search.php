<?php
/* @var $this PrestamosController */
/* @var $model Prestamos */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_prestamo'); ?>
		<?php echo $form->textField($model,'id_prestamo'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_solicitante'); ?>
		<?php echo $form->textField($model,'id_solicitante'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'monto'); ?>
		<?php echo $form->textField($model,'monto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad_cuotas'); ?>
		<?php echo $form->textField($model,'cantidad_cuotas'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_registro'); ?>
		<?php echo $form->textField($model,'fecha_registro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_autorizacion'); ?>
		<?php echo $form->textField($model,'fecha_autorizacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_registro'); ?>
		<?php echo $form->textField($model,'usuario_registro'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_autorizacion'); ?>
		<?php echo $form->textField($model,'usuario_autorizacion'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_estatus'); ?>
		<?php echo $form->textField($model,'id_estatus'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_tentativa_entrega'); ?>
		<?php echo $form->textField($model,'fecha_tentativa_entrega'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fecha_entrega'); ?>
		<?php echo $form->textField($model,'fecha_entrega'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'usuario_entrega'); ?>
		<?php echo $form->textField($model,'usuario_entrega'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->