<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'=>array('admin'),
	'Consultar Préstamos'
);

$this->menu=array(
	array('label'=>'Estatus General', 'url'=>array('general')),
	array('label'=>'Listado de Préstamos', 'url'=>array('admin')),
	array('label'=>'Registrar Préstamos', 'url'=>array('create')),
);

if($model->id_estatus == 1){
	$this->menu[]=array('label'=>'Autorizar Préstamos', 'url'=>array('autorizar', 'id'=>$model->id_prestamo));
}

if($model->id_estatus == 2){
	$this->menu[]=array('label'=>'Entregar Préstamos', 'url'=>array('entregar', 'id'=>$model->id_prestamo));
}

if($model->id_estatus == 3){
	$this->menu[]=array('label'=>'Registrar Pago de Cuotas', 'url'=>array('cuotas', 'id'=>$model->id_prestamo));
}
?>


<h1>Consultar Préstamos</h1>

<?php echo $this->renderPartial("_viewPrestamo",compact('model')); ?>


