<table class="table table-bordered">
	<tr class="active">
		<th>Nro. Préstamo</th>
		<th>Identificación</th>
		<th>Nombre y Apellido</th>
	</tr>

	<tr>
		<td><?php echo $model->id_prestamo ?></td>
		<td><?php echo $model->idSolicitante->identificacion ?></td>
		<td><?php echo $model->idSolicitante->nombreCompleto ?></td>
	</tr>

	<tr class="active">
		<th>Monto Total</th>
		<th>Nro. Cuotas</th>
		<th>Monto por Cuotas</th>
	</tr>

	<tr>
		<td><?php echo number_format($model->monto, 2, ',', '.') ?></td>
		<td><?php echo $model->cantidad_cuotas ?></td>
		<td><?php echo number_format($model->montoCuotas(), 2, ',', '.'); ?></td>
	</tr>

	<tr class="active">
		<th>Estatus</th>
		<th>Fecha Registro</th>
		<th>Usuario Registro</th>
	</tr>

	<tr>
		<td><?php echo $model->idEstatus->estatus; ?></td>
		<td><?php echo date("d/m/Y h:i a",strtotime($model->fecha_registro)) ?></td>
		<td><?php echo $model->idUsuarioRegistro->username; ?></td>
	</tr>

	<tr class="active">
		<th>Fecha Autorización</th>
		<th>Usuario Autorización</th>
		<th>Fecha Tentativa Entrega</th>
	</tr>

	<tr>
		<td><?php echo !empty($model->fecha_autorizacion)?date("d/m/Y h:i a",strtotime($model->fecha_autorizacion)):'N/A'; ?></td>
		<td><?php echo !empty($model->usuario_autorizacion)?$model->idUsuarioAutorizacion->username:'N/A'; ?></td>
		<td><?php echo !empty($model->fecha_tentativa_entrega)?date("d/m/Y",strtotime($model->fecha_tentativa_entrega)):'N/A'; ?></td>
	</tr>

	<tr class="active">
		<th>Fecha Entrega</th>
		<th>Usuario Entrega</th>
	</tr>

	<tr>
		<td><?php echo !empty($model->fecha_entrega)?date("d/m/Y h:i a",strtotime($model->fecha_entrega)):'N/A'; ?></td>
		<td><?php echo !empty($model->usuario_entrega)?$model->idUsuarioEntrega->username:'N/A'; ?></td>
	</tr>
</table>

<?php if($model->id_estatus == 3 || $model->id_estatus == 4){ ?>
	<br>
	<table class="table table-bordered">
		<tr class="active">
			<th class="centrar" colspan="6">Detalles de la Cuotas</th>
		</tr>

		<tr class="active">
			<th>Nro</th>
			<th>Monto</th>
			<th>Fecha Máxima de Pago</th>
			<th>Fecha de Pago</th>
			<th>Días Retraso de Pago</th>
			<th>Usuario</th>
		</tr>

		<?php
		$cuotas=$model->getDetallesCuotas();
		foreach ($cuotas as $nro => $cuota){
			?>
			<tr>
				<td class="centrar"><?php echo $nro; ?></td>
				<td><?php echo $cuota['monto']; ?></td>
				<td><?php echo $cuota['fechaMaxima'] ?></td>
				<td><?php echo isset($cuota['fechaPago'])?$cuota['fechaPago']:''; ?></td>
				<td class="centrar">
					<?php 
					if(isset($cuota['diasRetraso'])){
						if($cuota['diasRetraso'] > 0){
							echo "<span class='letra-roja negrita'>".$cuota['diasRetraso']."</span>";
						}else{
							echo $cuota['diasRetraso'];
						}
					} 
					?>
				</td>
				<td><?php echo isset($cuota['usuario'])?$cuota['usuario']:''; ?></td>
			</tr>
			<?php
		}
		?>
	</table>
<?php }else{

	echo "<br><div class='alert alert-danger'>Para visualizar los datos de las cuotas a cancelar el préstamo debe tener estatus entregado o pagado.</div>";
} ?>