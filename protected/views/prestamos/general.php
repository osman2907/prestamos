<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'=>array('admin'),
	'Estatus General'
);

$this->menu=array(
	array('label'=>'Listado de Préstamos', 'url'=>array('admin')),
	array('label'=>'Registrar Préstamos', 'url'=>array('create')),
);
?>


<h1>Estatus General</h1>

<table class="table table-bordered">
	<tr>
		<th class="active">Monto máximo para préstamos</th>
		<td><?php echo number_format($modelConfig->maximo_prestamos, 2, ',', '.'); ?></td>
	</tr>
	
	<tr>
		<th class="active">Monto prestado actualmente</th>
		<td><?php echo number_format($prestadoActual, 2, ',', '.'); ?></td>
	</tr>

	<tr>
		<th class="active">Disponible para prestar</th>
		<td><?php echo number_format($disponible, 2, ',', '.'); ?></td>
	</tr>

</table>


