<?php
/* @var $this PrestamosController */
/* @var $data Prestamos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_prestamo')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_prestamo), array('view', 'id'=>$data->id_prestamo)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_solicitante')); ?>:</b>
	<?php echo CHtml::encode($data->id_solicitante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monto')); ?>:</b>
	<?php echo CHtml::encode($data->monto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad_cuotas')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad_cuotas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_registro')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_registro); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_autorizacion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_autorizacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_registro')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_registro); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_autorizacion')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_autorizacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_estatus')); ?>:</b>
	<?php echo CHtml::encode($data->id_estatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_tentativa_entrega')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_tentativa_entrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_entrega')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_entrega); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_entrega')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_entrega); ?>
	<br />

	*/ ?>

</div>