<table border="0">
	<tr>
		<td style="text-align: center"><img src="https://www.ilernus.com/img/images/logo.png"></td>
	</tr>

	<tr>
		<td>
			<h1>Sistema de Gestión de Préstamos</h1>

			<p style="text-align: justify;">Saludos estimado(a) <b><?php echo $model->idSolicitante->nombreCompleto?></b>, el presente mensaje notifica que usted ha cancelado la última cuota de su préstamo número <b><?php echo $model->id_prestamo ?></b>, el mismo se encuentra en estatus <b>pagado</b> a partir de este momento, el pago de esta cuota generó el recibo número <b><?php echo $modelCuotas->id_prestamo_cuota ?></b>, si desea más información lo/la invitamos a ingresar al sistema.</b>
		</td>
	</tr>
</table>