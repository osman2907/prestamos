<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'=>array('admin'),
	'Rechazar Préstamos'
);

$this->menu=array(
	array('label'=>'Estatus General', 'url'=>array('general')),
	array('label'=>'Listado de Préstamos', 'url'=>array('admin')),
	array('label'=>'Registrar Préstamos', 'url'=>array('create')),
	array('label'=>'Consultar Préstamos', 'url'=>array('view', 'id'=>$model->id_prestamo)),
	array('label'=>'Autorizar Préstamos', 'url'=>array('autorizar', 'id'=>$model->id_prestamo))
);
?>

<h1>Rechazar Préstamos</h1>

<div class="alert alert-danger">
	Presione el siguiente botón si está seguro de rechazar este préstamo
</div>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prestamos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->hiddenField($model,'id_prestamo',array()); ?>

	<?php echo CHtml::submitButton('Rechazar',array('class'=>'btn btn-primary')); ?>

	<br><br>

<?php $this->endWidget(); ?>

<?php echo $this->renderPartial("_viewPrestamo",compact('model')); ?>


