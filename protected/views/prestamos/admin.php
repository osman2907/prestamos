<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'
);

$this->menu=array(
	array('label'=>'Estatus General', 'url'=>array('general')),
    array('label'=>'Registrar Préstamos', 'url'=>array('create'))
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#prestamos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

var decorarGrilla=function(){
	$('.grid-view .items thead tr:first-child th:first-child').addClass('brsi');
    $('.grid-view .items thead tr:first-child th:last-child').addClass('brsd');
    $('.grid-view .items thead .filters td input').addClass('form-control');
    $('.grid-view .items thead .filters td select').addClass('form-control');
}
decorarGrilla();
");
?>

<h1>Listado de Préstamos</h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'prestamos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'afterAjaxUpdate' => 'decorarGrilla',
	'columns'=>array(
		'id_prestamo',
		array(
			'name'=>'identificacion',
			'value'=>'$data->idSolicitante->identificacion'
		),
		array(
			'name'=>'nombreCompleto',
			'value'=>'$data->idSolicitante->nombreCompleto',
		),
		'monto',
		array(
			'name'=>'fecha_registro',
			'value'=>'date("d/m/Y",strtotime($data->fecha_registro))'
		),
		array(
			'name'=>'id_estatus',
			'value'=>'$data->idEstatus->estatus',
			'filter'=>$listaEstatus
		),
		array(
            'header'=>'Acciones',
            "type"=>'raw',
            'value'=>function($data){
                $ruta=Yii::app()->request->baseUrl."/index.php";
                $idPrestamo=$data->id_prestamo;

                $autorizar=$data->id_estatus != 1?'ocultar':'';
                $rechazar=$data->id_estatus != 1?'ocultar':'';
                $entregar=$data->id_estatus != 2?'ocultar':'';
                $cuotas=$data->id_estatus != 3?'ocultar':'';
                
                $html="
                    <div class='dropdown'>
                        <button class='btn btn-default dropdown-toggle type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'>
                            <i class='glyphicon glyphicon-cog'></i>
                            <span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu' aria-labelledby='dropdownMenu1' style='text-align:left'>
                            <li>
                                <a href='$ruta/prestamos/view/$idPrestamo'>
                                    <i class='glyphicon glyphicon-eye-open'></i> Consultar
                                </a>
                            </li>

                            <li class='$autorizar'>
                                <a href='$ruta/prestamos/autorizar/$idPrestamo'>
                                    <i class='glyphicon glyphicon-thumbs-up'></i> Autorizar
                                </a>
                            </li>

                            <li class='$rechazar'>
                                <a href='$ruta/prestamos/rechazar/$idPrestamo'>
                                    <i class='glyphicon glyphicon-remove'></i> Rechazar
                                </a>
                            </li>

                            <li class='$entregar'>
                                <a href='$ruta/prestamos/entregar/$idPrestamo'>
                                    <i class='glyphicon glyphicon-usd'></i> Entregar
                                </a>
                            </li>

                            <li class='$cuotas'>
                                <a href='$ruta/prestamos/cuotas/$idPrestamo'>
                                    <i class='glyphicon glyphicon-plus'></i> Registrar Pago de Cuotas
                                </a>
                            </li>
                        </ul>
                    </div>
                ";
                return $html;
            },
            'htmlOptions'=>array('style'=>'text-align:center; width:80px;'),
        ),
	),
)); ?>
