<script type="text/javascript">
	$(document).ready(function(){
		$(document).on("click","#btn-registrar-pago",function(){
			bootbox.confirm("¿Seguro que desea registrar este pago?",function(respuesta){
				if(respuesta){
					$("#prestamos-form").submit();
				}
			});
			return false;
		})
	});
</script>

<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'=>array('admin'),
	'Registrar Pago de Cuotas'
);

$this->menu=array(
	array('label'=>'Estatus General', 'url'=>array('general')),
	array('label'=>'Listado de Préstamos', 'url'=>array('admin')),
	array('label'=>'Registrar Préstamos', 'url'=>array('create')),
	array('label'=>'Consultar Préstamos', 'url'=>array('view', 'id'=>$model->id_prestamo))
);
?>

<h1>Registrar Pago de Cuotas</h1>


<div class="alert alert-info">
	El siguiente formulario le permitirá registrar las cuotas de los préstamos, en la parte inferior podrá visualizar los detalles del mismo.
</div>


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prestamos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->hiddenField($model,'id_prestamo',array()); ?>

	<div class="row">
		<div class="col-md-4">
			<?php echo $form->labelEx($modelCuotas,'monto'); ?>
			<?php echo $form->textField($modelCuotas,'monto',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($modelCuotas,'monto'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'monto'); ?>
			<?php echo $form->textField($model,'monto',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'monto'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'cuotas_canceladas'); ?>
			<?php echo $form->textField($model,'cuotas_canceladas',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'cuotas_canceladas'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'cuotas_faltantes'); ?>
			<?php echo $form->textField($model,'cuotas_faltantes',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'cuotas_faltantes'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'monto_cancelado'); ?>
			<?php echo $form->textField($model,'monto_cancelado',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'monto_cancelado'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'monto_faltante'); ?>
			<?php echo $form->textField($model,'monto_faltante',array('class'=>'form-control','readonly'=>'readonly')); ?>
			<?php echo $form->error($model,'monto_faltante'); ?>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-4">
			<?php echo CHtml::submitButton('Registrar Pago',array('class'=>'btn btn-primary','id'=>'btn-registrar-pago')); ?>
		</div>
	</div>
	<br>

<?php $this->endWidget(); ?>


<?php echo $this->renderPartial("_viewPrestamo",compact('model')); ?>


