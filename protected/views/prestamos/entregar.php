<?php
$this->breadcrumbs=array(
	'Listado de Préstamos'=>array('admin'),
	'Entregar Préstamos'
);

$this->menu=array(
	array('label'=>'Estatus General', 'url'=>array('general')),
	array('label'=>'Listado de Préstamos', 'url'=>array('admin')),
	array('label'=>'Registrar Préstamos', 'url'=>array('create')),
	array('label'=>'Consultar Préstamos', 'url'=>array('view', 'id'=>$model->id_prestamo))
);
?>

<h1>Entregar Préstamos</h1>

<?php if($model->fecha_tentativa_entrega <= date("Y-m-d")){ ?>
	<div class="alert alert-danger">
		Presione el siguiente botón si está seguro de entregar este préstamo
	</div>


	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'prestamos-form',
		'enableAjaxValidation'=>false,
	)); ?>

		<?php echo $form->hiddenField($model,'id_prestamo',array()); ?>
		<?php echo CHtml::submitButton('Entregar',array('class'=>'btn btn-primary')); ?>
		<br><br>

	<?php $this->endWidget(); ?>
<?php }else{?>
	<div class="alert alert-danger">
		Para entregar el préstamo debe esperar la fecha tentativa de entrega del mismo.
	</div>
<?php } ?>

<?php echo $this->renderPartial("_viewPrestamo",compact('model')); ?>


