<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'prestamos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-4">
			<?php echo $form->labelEx($modelSolicitante,'identificacion'); ?>
			<?php echo $form->hiddenField($model,'id_solicitante'); ?>
			<?php echo $form->textField($modelSolicitante,'identificacion',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($modelSolicitante,'primer_nombre'); ?>
			<?php echo $form->textField($modelSolicitante,'primer_nombre',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($modelSolicitante,'primer_apellido'); ?>
			<?php echo $form->textField($modelSolicitante,'primer_apellido',array('class'=>'form-control', 'readonly'=>'readonly')); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-4">
			<?php echo $form->labelEx($model,'monto'); ?>
			<?php echo $form->textField($model,'monto',array('class'=>'form-control')); ?>
			<?php echo $form->error($model,'monto'); ?>
		</div>

		<div class="col-md-4">
			<?php echo $form->labelEx($model,'cantidad_cuotas'); ?>
			<?php echo $form->dropDownList($model,'cantidad_cuotas',$listaCuotas,array('class'=>'form-control','empty'=>'Seleccione')); ?>
			<?php echo $form->error($model,'cantidad_cuotas'); ?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Registrar' : 'Modificar',array('class'=>$model->isNewRecord ? 'btn btn-success' : 'btn btn-primary')); ?>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->