<table border="0">
	<tr>
		<td style="text-align: center"><img src="https://www.ilernus.com/img/images/logo.png"></td>
	</tr>

	<tr>
		<td>
			<h1>Sistema de Gestión de Préstamos</h1>

			<p style="text-align: justify;">Saludos estimado(a) <b><?php echo $model->idSolicitante->nombreCompleto?></b>, el presente mensaje notifica que su solicitud de préstamo número <b><?php echo $model->id_prestamo ?></b> se encuentra en estatus <b><?php echo $model->idEstatus->estatus ?></b>, para más información lo/la invitamos a ingresar al sistema.</p>
		</td>
	</tr>
</table>