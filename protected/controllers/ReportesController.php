<?php
class ReportesController extends Controller{

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	public function actionIndex(){
		$modelRep=new Reportes;
		$listaAnios=$modelRep->getListaAnios();
		$listaMeses=$modelRep->getListaMeses();
		if(isset($_POST['anio'])){
			$anioReporte=$_POST['anio'];
			$data=$modelRep->getGraficoCantidades($anioReporte);
			$dataMontos=$modelRep->getGraficoMontos($anioReporte);
		}
		$this->render('index',compact('listaAnios','listaMeses','data','dataMontos','anioReporte'));
	}


	public function actionGeneral(){
		$anio=$_POST['anio'];
		$this->renderPartial("_general",compact('anio'));
	}

}