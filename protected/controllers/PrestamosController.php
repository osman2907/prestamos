<?php
class PrestamosController extends Controller{
	
	public $layout='//layouts/column2';


	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	
	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate($idSolicitante=null){
		$model=new Prestamos;
		$model->id_solicitante=!is_null($idSolicitante)?$idSolicitante:'';

		// $this->performAjaxValidation($model);

		if(isset($_POST['Prestamos'])){
			$_POST=$model->ordenarRegistro($_POST);
			$model->attributes=$_POST['Prestamos'];
			if(!$model->validarDisponible()){
				Yii::app()->user->setFlash('error', "El préstamo que está solicitando supera el monto máximo a prestar, Por favor intente con un monto menor");
			}else{
				
				if($model->save()){
					$mensaje=$this->renderPartial("_notificaciones",compact('model'),true);
					$this->enviarCorreo($model->idSolicitante->email,Yii::app()->name,$mensaje);
					$this->redirect(array('view','id'=>$model->id_prestamo));
				}
			}
		}


		if(is_null($idSolicitante)){
			$this->render('createBuscarSolicitante');
		}else{
			$modelSolicitante=Solicitantes::model()->find(array('condition'=>"id_solicitante=$idSolicitante"));
			if(!$modelSolicitante){
				$this->redirect(array('create'));
			}
			$listaCuotas=Configuraciones::model()->listaCuotas();
			$this->render('create',compact('model','modelSolicitante','listaCuotas'));	
		}
		
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Prestamos'])){
			$model->attributes=$_POST['Prestamos'];
			if($model->save()){
				$this->redirect(array('view','id'=>$model->id_prestamo));
			}
		}

		$this->render('update',compact('model'));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	/*public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Prestamos');
		$this->render('index',compact('dataProvider'));
	}*/

	
	public function actionAdmin(){
		$model=new Prestamos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Prestamos']))
			$model->attributes=$_GET['Prestamos'];

		$listaEstatus=PrestamosEstatus::model()->getListaEstatus();

		$this->render('admin',compact('model','listaEstatus'));
	}


	public function actionAutorizar($id){
		$model=$this->loadModel($id);
		if($model->id_estatus != 1){
			Yii::app()->user->setFlash('error', "El préstamo tiene un estatus que no permite esta acción");
			$this->redirect(array('admin'));
		}

		$dia=date("d");
		//$dia=21;
		if($dia > 20){
			Yii::app()->user->setFlash('error', "Sólo se pueden autorizar préstamos los primeros 20 días de cada mes.");
			$this->redirect(array('admin'));
		}		

		if(isset($_POST['Prestamos'])){
			$_POST=$model->ordenarAutorizacion($_POST);
			$model->attributes=$_POST['Prestamos'];
			if($model->save()){
				$mensaje=$this->renderPartial("_notificaciones",compact('model'),true);
				$this->enviarCorreo($model->idSolicitante->email,Yii::app()->name,$mensaje);
				Yii::app()->user->setFlash('success', "Préstamo autorizado con éxito");
				$this->redirect(array('view','id'=>$model->id_prestamo));
			}
		}

		$this->render('autorizar',compact('model'));
	}


	public function actionRechazar($id){
		$model=$this->loadModel($id);
		if($model->id_estatus != 1){
			Yii::app()->user->setFlash('error', "El préstamo tiene un estatus que no permite esta acción");
			$this->redirect(array('admin'));
		}

		if(isset($_POST['Prestamos'])){
			$_POST=$model->ordenarRechazo($_POST);
			$model->attributes=$_POST['Prestamos'];

			if($model->save()){
				$mensaje=$this->renderPartial("_notificaciones",compact('model'),true);
				$this->enviarCorreo($model->idSolicitante->email,Yii::app()->name,$mensaje);
				Yii::app()->user->setFlash('success', "Préstamo rechazado");
				$this->redirect(array('view','id'=>$model->id_prestamo));
			}
		}

		$this->render('rechazar',compact('model'));
	}


	public function actionEntregar($id){
		$model=$this->loadModel($id);
		if($model->id_estatus != 2){
			Yii::app()->user->setFlash('error', "El préstamo tiene un estatus que no permite esta acción");
			$this->redirect(array('admin'));
		}

		if(isset($_POST['Prestamos'])){
			if($model->fecha_tentativa_entrega > date("Y-m-d")){
				Yii::app()->user->setFlash('error', "Para entregar el préstamo debe esperar la fecha tentativa de entrega del mismo.");
				$this->redirect(array('admin'));
			}
			$_POST=$model->ordenarEntrega($_POST);
			$model->attributes=$_POST['Prestamos'];
			if($model->save()){
				$mensaje=$this->renderPartial("_notificaciones",compact('model'),true);
				$this->enviarCorreo($model->idSolicitante->email,Yii::app()->name,$mensaje);
				Yii::app()->user->setFlash('success', "Préstamo entregado con éxito");
				$this->redirect(array('view','id'=>$model->id_prestamo));
			}
		}

		$this->render('entregar',compact('model'));
	}


	public function actionCuotas($id){
		$model=$this->loadModel($id);
		if($model->id_estatus != 3){
			Yii::app()->user->setFlash('error', "El préstamo tiene un estatus que no permite esta acción");
			$this->redirect(array('admin'));
		}

		$modelCuotas=new PrestamosCuotas;
		$modelCuotas->id_prestamo=$id;
		$modelCuotas->monto=number_format($model->montoCuotas(), 2, '.', '');

		$model->cuotas_canceladas=count($model->prestamosCuotas);
		$model->cuotas_faltantes=$model->getCuotasFaltantes();
		$model->monto_cancelado=$model->getMontoCancelado();
		$model->monto_faltante=$model->getMontoFaltante();

		if(isset($_POST['PrestamosCuotas'])){
			$_POST=$modelCuotas->ordenarPago($_POST);
			$modelCuotas->attributes=$_POST['PrestamosCuotas'];
			if($modelCuotas->save()){
				if($model->validarPagoTotal()){
					$mensaje=$this->renderPartial("_notificacionesCuotaFinal",compact('model','modelCuotas'),true);
				}else{
					$mensaje=$this->renderPartial("_notificacionesCuota",compact('model','modelCuotas'),true);
				}
				$this->enviarCorreo($model->idSolicitante->email,Yii::app()->name,$mensaje);
				Yii::app()->user->setFlash('success', "Pago de cuota registrado con éxito");
				$this->redirect(array('view','id'=>$model->id_prestamo));
			}
		}

		$this->render('cuotas',compact('model','modelCuotas'));
	}

	
	public function loadModel($id){
		$model=Prestamos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


	public function actionBuscarSolicitante(){
		$identificacion=$_POST['identificacion'];
		$modelSolicitante=Solicitantes::model()->find(array('condition'=>"identificacion=$identificacion"));
		echo $modelSolicitante?$modelSolicitante->id_solicitante:false;
	}


	public function actionGeneral(){
		$model=new Prestamos;
		$prestadoActual=$model->getPrestadoActual();
		$disponible=$model->getMontoDisponible();
		$modelConfig=Configuraciones::model()->findByPk(1);
		$this->render("general",compact('modelConfig','prestadoActual','disponible'));
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='prestamos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function actionMail(){
		$mensaje = "Línea 1\r\nLínea 2\r\nLínea 3";
		$mensaje = wordwrap($mensaje, 70, "\r\n");
		$this->enviarCorreo("osman2907@gmail.com","Prueba de Envíos",$mensaje);
	}

}
