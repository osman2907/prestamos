<?php
class SolicitantesController extends Controller{
	
	public $layout='//layouts/column2';

	
	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	
	public function actionView($id){
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	public function actionCreate($identificacion = null){
		$model=new Solicitantes;
		$model->identificacion=!is_null($identificacion)?$identificacion:'';

		// $this->performAjaxValidation($model);

		if(isset($_POST['Solicitantes'])){
			$model->attributes=$_POST['Solicitantes'];
			if($model->save()){
				if(!is_null($identificacion)){
					$this->redirect(array('/prestamos/create','idSolicitante'=>$model->id_solicitante));
				}else{
					$this->redirect(array('view','id'=>$model->id_solicitante));
				}
			}
		}

		$this->render('create',compact('model'));
	}

	
	public function actionUpdate($id){
		$model=$this->loadModel($id);

		// $this->performAjaxValidation($model);

		if(isset($_POST['Solicitantes'])){
			$model->attributes=$_POST['Solicitantes'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id_solicitante));
		}

		$this->render('update',compact('model'));
	}

	
	public function actionDelete($id){
		$this->loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	/*public function actionIndex(){
		$dataProvider=new CActiveDataProvider('Solicitantes');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}*/

	
	public function actionAdmin(){
		$model=new Solicitantes('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Solicitantes']))
			$model->attributes=$_GET['Solicitantes'];

		$this->render('admin',compact('model'));
	}

	
	public function loadModel($id){
		$model=Solicitantes::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	
	protected function performAjaxValidation($model){
		if(isset($_POST['ajax']) && $_POST['ajax']==='solicitantes-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
