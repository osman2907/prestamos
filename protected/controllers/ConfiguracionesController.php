<?php
class ConfiguracionesController extends Controller{

	public function filters(){
		return array(array('CrugeAccessControlFilter'));
	}

	public function actionIndex(){
		$model=Configuraciones::model()->findByPk(1);

		if(isset($_POST['Configuraciones'])){
			$model->attributes=$_POST['Configuraciones'];
			if($model->save()){
				Yii::app()->user->setFlash('success', "Configuraciones guardadas con éxito");
			}
		}


		$this->render('index',compact('model'));
	}
	
}