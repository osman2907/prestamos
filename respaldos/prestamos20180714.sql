-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 15-07-2018 a las 00:36:13
-- Versión del servidor: 5.6.33-0ubuntu0.14.04.1
-- Versión de PHP: 5.6.36-1+ubuntu14.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prestamos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

CREATE TABLE `configuraciones` (
  `id_configuracion` int(11) NOT NULL,
  `maximo_cuotas` int(11) NOT NULL,
  `maximo_prestamos` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`id_configuracion`, `maximo_cuotas`, `maximo_prestamos`) VALUES
(1, 6, 15000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_authassignment`
--

CREATE TABLE `cruge_authassignment` (
  `userid` int(11) NOT NULL,
  `bizrule` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci,
  `itemname` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_authitem`
--

CREATE TABLE `cruge_authitem` (
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `bizrule` text COLLATE utf8mb4_unicode_ci,
  `data` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cruge_authitem`
--

INSERT INTO `cruge_authitem` (`name`, `type`, `description`, `bizrule`, `data`) VALUES
('action_configuraciones_index', 0, '', NULL, 'N;'),
('action_prestamos_admin', 0, '', NULL, 'N;'),
('action_prestamos_autorizar', 0, '', NULL, 'N;'),
('action_prestamos_buscarSolicitante', 0, '', NULL, 'N;'),
('action_prestamos_create', 0, '', NULL, 'N;'),
('action_prestamos_cuotas', 0, '', NULL, 'N;'),
('action_prestamos_entregar', 0, '', NULL, 'N;'),
('action_prestamos_general', 0, '', NULL, 'N;'),
('action_prestamos_index', 0, '', NULL, 'N;'),
('action_prestamos_mail', 0, '', NULL, 'N;'),
('action_prestamos_rechazar', 0, '', NULL, 'N;'),
('action_prestamos_validarSolicitante', 0, '', NULL, 'N;'),
('action_prestamos_view', 0, '', NULL, 'N;'),
('action_reportes_general', 0, '', NULL, 'N;'),
('action_reportes_index', 0, '', NULL, 'N;'),
('action_solicitantes_admin', 0, '', NULL, 'N;'),
('action_solicitantes_create', 0, '', NULL, 'N;'),
('action_solicitantes_delete', 0, '', NULL, 'N;'),
('action_solicitantes_update', 0, '', NULL, 'N;'),
('action_solicitantes_view', 0, '', NULL, 'N;'),
('action_ui_usermanagementadmin', 0, '', NULL, 'N;'),
('action_ui_usermanagementcreate', 0, '', NULL, 'N;'),
('action_ui_usermanagementupdate', 0, '', NULL, 'N;'),
('admin', 0, '', NULL, 'N;'),
('edit-advanced-profile-features', 0, '/home/osman/www/pruebas/prestamos/protected/modules/cruge/views/ui/usermanagementupdate.php linea 114', NULL, 'N;');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_authitemchild`
--

CREATE TABLE `cruge_authitemchild` (
  `parent` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_field`
--

CREATE TABLE `cruge_field` (
  `idfield` int(11) NOT NULL,
  `fieldname` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `longname` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` int(11) DEFAULT '0',
  `fieldtype` int(11) DEFAULT '0',
  `fieldsize` int(11) DEFAULT '20',
  `maxlength` int(11) DEFAULT '45',
  `showinreports` int(11) DEFAULT '0',
  `useregexp` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useregexpmsg` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `predetvalue` mediumblob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_fieldvalue`
--

CREATE TABLE `cruge_fieldvalue` (
  `idfieldvalue` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idfield` int(11) NOT NULL,
  `value` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_session`
--

CREATE TABLE `cruge_session` (
  `idsession` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `created` bigint(30) DEFAULT NULL,
  `expire` bigint(30) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `ipaddress` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usagecount` int(11) DEFAULT '0',
  `lastusage` bigint(30) DEFAULT NULL,
  `logoutdate` bigint(30) DEFAULT NULL,
  `ipaddressout` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cruge_session`
--

INSERT INTO `cruge_session` (`idsession`, `iduser`, `created`, `expire`, `status`, `ipaddress`, `usagecount`, `lastusage`, `logoutdate`, `ipaddressout`) VALUES
(1, 1, 1531442773, 1531444576, 0, '127.0.0.1', 1, 1531442773, 1531442776, '127.0.0.1'),
(2, 1, 1531442783, 1531444807, 0, '127.0.0.1', 1, 1531442783, 1531443007, '127.0.0.1'),
(3, 1, 1531443014, 1531467570, 0, '127.0.0.1', 1, 1531443014, 1531465771, '127.0.0.1'),
(4, 1, 1531507644, 1531514583, 1, '127.0.0.1', 1, 1531507644, NULL, NULL),
(5, 1, 1531515213, 1531518245, 1, '127.0.0.1', 1, 1531515213, NULL, NULL),
(6, 1, 1531576861, 1531580317, 1, '127.0.0.1', 1, 1531576861, NULL, NULL),
(7, 1, 1531597798, 1531602353, 1, '127.0.0.1', 1, 1531597798, NULL, NULL),
(8, 1, 1531602738, 1531609492, 0, '127.0.0.1', 1, 1531602738, 1531607692, '127.0.0.1'),
(9, 1, 1531607700, 1531627327, 0, '192.168.0.101', 2, 1531625372, 1531625528, '192.168.0.101');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_system`
--

CREATE TABLE `cruge_system` (
  `idsystem` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `largename` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sessionmaxdurationmins` int(11) DEFAULT '30',
  `sessionmaxsameipconnections` int(11) DEFAULT '10',
  `sessionreusesessions` int(11) DEFAULT '1' COMMENT '1yes 0no',
  `sessionmaxsessionsperday` int(11) DEFAULT '-1',
  `sessionmaxsessionsperuser` int(11) DEFAULT '-1',
  `systemnonewsessions` int(11) DEFAULT '0' COMMENT '1yes 0no',
  `systemdown` int(11) DEFAULT '0',
  `registerusingcaptcha` int(11) DEFAULT '0',
  `registerusingterms` int(11) DEFAULT '0',
  `terms` blob,
  `registerusingactivation` int(11) DEFAULT '1',
  `defaultroleforregistration` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registerusingtermslabel` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `registrationonlogin` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cruge_system`
--

INSERT INTO `cruge_system` (`idsystem`, `name`, `largename`, `sessionmaxdurationmins`, `sessionmaxsameipconnections`, `sessionreusesessions`, `sessionmaxsessionsperday`, `sessionmaxsessionsperuser`, `systemnonewsessions`, `systemdown`, `registerusingcaptcha`, `registerusingterms`, `terms`, `registerusingactivation`, `defaultroleforregistration`, `registerusingtermslabel`, `registrationonlogin`) VALUES
(1, 'default', NULL, 30, 10, 1, -1, -1, 0, 0, 0, 0, '', 0, '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cruge_user`
--

CREATE TABLE `cruge_user` (
  `iduser` int(11) NOT NULL,
  `regdate` bigint(30) DEFAULT NULL,
  `actdate` bigint(30) DEFAULT NULL,
  `logondate` bigint(30) DEFAULT NULL,
  `username` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Hashed password',
  `authkey` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'llave de autentificacion',
  `state` int(11) DEFAULT '0',
  `totalsessioncounter` int(11) DEFAULT '0',
  `currentsessioncounter` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cruge_user`
--

INSERT INTO `cruge_user` (`iduser`, `regdate`, `actdate`, `logondate`, `username`, `email`, `password`, `authkey`, `state`, `totalsessioncounter`, `currentsessioncounter`) VALUES
(1, NULL, NULL, 1531625373, 'admin', 'admin@tucorreo.com', '25d55ad283aa400af464c76d713c07ad', NULL, 1, 0, 0),
(2, NULL, NULL, NULL, 'invitado', 'invitado', 'nopassword', NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamos`
--

CREATE TABLE `prestamos` (
  `id_prestamo` int(11) NOT NULL,
  `id_solicitante` int(11) NOT NULL,
  `monto` float NOT NULL,
  `cantidad_cuotas` int(11) NOT NULL,
  `fecha_registro` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `fecha_autorizacion` datetime DEFAULT NULL,
  `usuario_registro` int(11) NOT NULL,
  `usuario_autorizacion` int(11) DEFAULT NULL,
  `id_estatus` int(11) NOT NULL,
  `fecha_tentativa_entrega` date DEFAULT NULL,
  `fecha_entrega` datetime DEFAULT NULL,
  `usuario_entrega` int(11) DEFAULT NULL,
  `fecha_rechazo` datetime DEFAULT NULL,
  `usuario_rechazo` int(11) DEFAULT NULL,
  `fecha_pagado` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `prestamos`
--

INSERT INTO `prestamos` (`id_prestamo`, `id_solicitante`, `monto`, `cantidad_cuotas`, `fecha_registro`, `fecha_autorizacion`, `usuario_registro`, `usuario_autorizacion`, `id_estatus`, `fecha_tentativa_entrega`, `fecha_entrega`, `usuario_entrega`, `fecha_rechazo`, `usuario_rechazo`, `fecha_pagado`) VALUES
(1, 5, 1000, 6, '2018-01-14 23:12:07', NULL, 1, NULL, 5, NULL, NULL, NULL, '2018-07-15 00:13:27', 1, NULL),
(2, 1, 5000, 5, '2018-07-14 22:51:56', '2018-01-13 03:09:03', 1, 1, 4, '2018-07-13', '2018-02-13 03:41:46', 1, NULL, NULL, '2018-03-08 13:00:00'),
(3, 6, 10000, 3, '2018-01-14 22:52:43', '2018-07-14 11:11:09', 1, 1, 4, '2018-07-14', '2018-07-14 11:15:47', 1, NULL, NULL, '2018-04-01 14:00:05'),
(4, 1, 3000, 6, '2018-07-14 22:53:21', '2018-07-14 18:36:17', 1, 1, 2, '2018-07-21', NULL, NULL, NULL, NULL, NULL),
(5, 5, 1500, 3, '2018-07-14 23:12:33', NULL, 1, NULL, 1, NULL, NULL, NULL, '2018-07-15 00:11:16', 1, NULL),
(6, 6, 1500, 5, '2018-07-14 23:12:39', NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 1, 3000, 6, '2018-07-14 22:53:14', '2018-07-14 20:45:57', 1, 1, 4, '2018-07-14', '2018-07-14 20:49:29', 1, NULL, NULL, '2018-07-14 09:05:10'),
(8, 6, 5000, 2, '2018-07-14 22:53:31', '2018-07-14 21:26:23', 1, 1, 4, '2018-07-14', '2018-07-14 21:27:34', 1, NULL, NULL, '2018-07-14 23:55:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamos_cuotas`
--

CREATE TABLE `prestamos_cuotas` (
  `id_prestamo_cuota` int(11) NOT NULL,
  `id_prestamo` int(11) NOT NULL,
  `monto` float NOT NULL,
  `fecha_pago` datetime NOT NULL,
  `usuario_registro` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `prestamos_cuotas`
--

INSERT INTO `prestamos_cuotas` (`id_prestamo_cuota`, `id_prestamo`, `monto`, `fecha_pago`, `usuario_registro`) VALUES
(1, 2, 1000, '2018-07-13 18:13:04', 1),
(2, 2, 1000, '2018-07-13 18:14:00', 1),
(3, 2, 1000, '2018-07-14 11:04:05', 1),
(8, 2, 1000, '2018-07-14 11:09:47', 1),
(9, 2, 1000, '2018-07-14 11:10:46', 1),
(10, 3, 3333.33, '2018-07-14 11:16:28', 1),
(11, 3, 3333.33, '2018-09-15 16:53:48', 1),
(12, 3, 3333.33, '2018-07-14 19:21:07', 1),
(13, 7, 500, '2018-07-14 20:58:03', 1),
(14, 7, 500, '2018-07-14 20:59:12', 1),
(15, 7, 500, '2018-07-14 21:01:46', 1),
(16, 7, 500, '2018-07-14 21:02:13', 1),
(17, 7, 500, '2018-07-14 21:06:24', 1),
(18, 7, 500, '2018-07-14 21:06:38', 1),
(19, 8, 2500, '2018-07-14 23:55:13', 1),
(20, 8, 2500, '2018-07-14 23:55:51', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prestamos_estatus`
--

CREATE TABLE `prestamos_estatus` (
  `id_estatus` int(11) NOT NULL,
  `estatus` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `prestamos_estatus`
--

INSERT INTO `prestamos_estatus` (`id_estatus`, `estatus`) VALUES
(1, 'Pendiente'),
(2, 'Autorizado'),
(3, 'Entregado'),
(4, 'Pagado'),
(5, 'Rechazado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitantes`
--

CREATE TABLE `solicitantes` (
  `id_solicitante` int(11) NOT NULL,
  `identificacion` int(11) NOT NULL,
  `primer_nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primer_apellido` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_local` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono_movil` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `solicitantes`
--

INSERT INTO `solicitantes` (`id_solicitante`, `identificacion`, `primer_nombre`, `primer_apellido`, `telefono_local`, `telefono_movil`, `email`) VALUES
(1, 19255285, 'Osman Orlando', 'Pérez', '02123228013', '04168016664', 'osman2907@gmail.com'),
(5, 6172106, 'Carmen', 'Martínez', '02128788221', '04263319775', 'carmenmartinez@gmail.com'),
(6, 15020279, 'Norifer', 'González', '02123228013', '04169308657', 'ngonzalez2424@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `cruge_authassignment`
--
ALTER TABLE `cruge_authassignment`
  ADD PRIMARY KEY (`userid`,`itemname`),
  ADD KEY `fk_cruge_authassignment_cruge_authitem1` (`itemname`),
  ADD KEY `fk_cruge_authassignment_user` (`userid`);

--
-- Indices de la tabla `cruge_authitem`
--
ALTER TABLE `cruge_authitem`
  ADD PRIMARY KEY (`name`);

--
-- Indices de la tabla `cruge_authitemchild`
--
ALTER TABLE `cruge_authitemchild`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indices de la tabla `cruge_field`
--
ALTER TABLE `cruge_field`
  ADD PRIMARY KEY (`idfield`);

--
-- Indices de la tabla `cruge_fieldvalue`
--
ALTER TABLE `cruge_fieldvalue`
  ADD PRIMARY KEY (`idfieldvalue`),
  ADD KEY `fk_cruge_fieldvalue_cruge_user1` (`iduser`),
  ADD KEY `fk_cruge_fieldvalue_cruge_field1` (`idfield`);

--
-- Indices de la tabla `cruge_session`
--
ALTER TABLE `cruge_session`
  ADD PRIMARY KEY (`idsession`),
  ADD KEY `crugesession_iduser` (`iduser`);

--
-- Indices de la tabla `cruge_system`
--
ALTER TABLE `cruge_system`
  ADD PRIMARY KEY (`idsystem`);

--
-- Indices de la tabla `cruge_user`
--
ALTER TABLE `cruge_user`
  ADD PRIMARY KEY (`iduser`);

--
-- Indices de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  ADD PRIMARY KEY (`id_prestamo`),
  ADD KEY `usuario_registro` (`usuario_registro`),
  ADD KEY `usuario_autorizacion` (`usuario_autorizacion`),
  ADD KEY `id_estatus` (`id_estatus`),
  ADD KEY `id_solicitante` (`id_solicitante`),
  ADD KEY `usuario_entrega` (`usuario_entrega`),
  ADD KEY `usuario_rechazo` (`usuario_rechazo`);

--
-- Indices de la tabla `prestamos_cuotas`
--
ALTER TABLE `prestamos_cuotas`
  ADD PRIMARY KEY (`id_prestamo_cuota`);

--
-- Indices de la tabla `prestamos_estatus`
--
ALTER TABLE `prestamos_estatus`
  ADD PRIMARY KEY (`id_estatus`);

--
-- Indices de la tabla `solicitantes`
--
ALTER TABLE `solicitantes`
  ADD PRIMARY KEY (`id_solicitante`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cruge_field`
--
ALTER TABLE `cruge_field`
  MODIFY `idfield` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cruge_fieldvalue`
--
ALTER TABLE `cruge_fieldvalue`
  MODIFY `idfieldvalue` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cruge_session`
--
ALTER TABLE `cruge_session`
  MODIFY `idsession` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `cruge_system`
--
ALTER TABLE `cruge_system`
  MODIFY `idsystem` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `cruge_user`
--
ALTER TABLE `cruge_user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `prestamos`
--
ALTER TABLE `prestamos`
  MODIFY `id_prestamo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `prestamos_cuotas`
--
ALTER TABLE `prestamos_cuotas`
  MODIFY `id_prestamo_cuota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `solicitantes`
--
ALTER TABLE `solicitantes`
  MODIFY `id_solicitante` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cruge_authassignment`
--
ALTER TABLE `cruge_authassignment`
  ADD CONSTRAINT `fk_cruge_authassignment_cruge_authitem1` FOREIGN KEY (`itemname`) REFERENCES `cruge_authitem` (`name`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cruge_authassignment_user` FOREIGN KEY (`userid`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `cruge_authitemchild`
--
ALTER TABLE `cruge_authitemchild`
  ADD CONSTRAINT `crugeauthitemchild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `crugeauthitemchild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `cruge_authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `cruge_fieldvalue`
--
ALTER TABLE `cruge_fieldvalue`
  ADD CONSTRAINT `fk_cruge_fieldvalue_cruge_field1` FOREIGN KEY (`idfield`) REFERENCES `cruge_field` (`idfield`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cruge_fieldvalue_cruge_user1` FOREIGN KEY (`iduser`) REFERENCES `cruge_user` (`iduser`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `prestamos`
--
ALTER TABLE `prestamos`
  ADD CONSTRAINT `prestamos_ibfk_1` FOREIGN KEY (`id_estatus`) REFERENCES `prestamos_estatus` (`id_estatus`),
  ADD CONSTRAINT `prestamos_ibfk_2` FOREIGN KEY (`id_solicitante`) REFERENCES `solicitantes` (`id_solicitante`),
  ADD CONSTRAINT `prestamos_ibfk_3` FOREIGN KEY (`usuario_autorizacion`) REFERENCES `cruge_user` (`iduser`),
  ADD CONSTRAINT `prestamos_ibfk_4` FOREIGN KEY (`usuario_entrega`) REFERENCES `cruge_user` (`iduser`),
  ADD CONSTRAINT `prestamos_ibfk_5` FOREIGN KEY (`usuario_rechazo`) REFERENCES `cruge_user` (`iduser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
